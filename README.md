# currencyAPI



## 啟動API Server
```
docker build -t 'currencyapi'
```

```
docker run -p 8080:8080 currencyapi 
```

## curl command 呼叫API
以下輸入分別為
來源幣別 {from}
目標幣別 {to}
金額數字 {sourceAmount}
```
curl 'http://localhost:8080/convert/q?from={from}&to={to}&sourceAmount={sourceAmount}' 
```

範例：
```
http://localhost:8080/convert/q?from=TWD&to=JPY&sourceAmount=1000
```

輸出： 
to_amount為輸出結果
```
{"to_amount":"3,669.00","error":""}
```
