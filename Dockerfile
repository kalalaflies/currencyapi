FROM        golang
RUN         mkdir -p /apiserver
WORKDIR     /apiserver
COPY        . .
RUN         go mod download
RUN         go build -o apiserver
ENTRYPOINT  ["./apiserver"]