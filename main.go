package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func main() {
	err := initExchangeRate()
	if err != nil {
		log.Fatalln("initialize: ")
	}

	r := mux.NewRouter()
	r.HandleFunc("/", Home)
	r.HandleFunc("/convert/q", Convert).Queries("from", "{from}", "to", "{to}", "sourceAmount", "{sourceAmount}")

	http.ListenAndServe(":8080", r)
}

func Home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("use curl command!"))
}

func Convert(w http.ResponseWriter, r *http.Request) {
	convertCurrencyRsp := convertCurrencyResponse{}

	vars := mux.Vars(r)
	from := vars["from"]
	to := vars["to"]
	exchangeRate, err := getExchangeRate(from, to)
	if err != nil {
		convertCurrencyRsp.Error = fmt.Sprint(err)
		responseWithJson(w, http.StatusCreated, convertCurrencyRsp)
		return
	}

	souceAmount, err := strconv.ParseFloat(vars["sourceAmount"], 64)
	if err != nil {
		convertCurrencyRsp.Error = fmt.Sprint(err)
		responseWithJson(w, http.StatusCreated, convertCurrencyRsp)
		return
	}

	toAmount, _ := decimal.NewFromFloat(souceAmount).Mul(decimal.NewFromFloat(exchangeRate)).Round(2).Float64()
	p := message.NewPrinter(language.English)
	resultAmount := p.Sprintf("%.2f", toAmount)

	convertCurrencyRsp = convertCurrencyResponse{
		ToAmount: resultAmount,
	}

	responseWithJson(w, http.StatusCreated, convertCurrencyRsp)

}

type convertCurrencyResponse struct {
	ToAmount string `json:"to_amount"`
	Error    string `json:"error"`
}

func responseWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

var currencyInfo currency

type currency struct {
	Currencies map[string]exchangeRate `json:"currencies"`
}

type exchangeRate struct {
	TWD float64 `json:"TWD"`
	JPY float64 `json:"JPY"`
	USD float64 `json:"USD"`
}

func initExchangeRate() error {
	data, err := ioutil.ReadFile("exchangeRate.json")

	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &currencyInfo)

	if nil != err {
		return err
	} else {
		log.Println("currency Info:", currencyInfo)
	}
	return nil
}

func getExchangeRate(from, to string) (float64, error) {

	if _, ok := currencyInfo.Currencies[from]; ok {
		switch to {
		case "TWD":
			return currencyInfo.Currencies[from].TWD, nil
		case "JPY":
			return currencyInfo.Currencies[from].JPY, nil
		case "USD":
			return currencyInfo.Currencies[from].USD, nil
		}
	}

	return 0, fmt.Errorf("不存在的幣別")

}
