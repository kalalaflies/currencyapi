module currencyExchangeAPI

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/shopspring/decimal v1.3.1
	golang.org/x/text v0.3.7
)
